function getTimeDiff(date_now,date_future){
	if(date_now>=date_future){return("0:0:0:0");}
	// get total seconds between the times
	var delta = Math.abs(date_future-date_now)/1000;
	// calculate (and subtract) whole days
	var days = Math.floor(delta/86400);
	delta -= days*86400;
	// calculate (and subtract) whole hours
	var hours = Math.floor(delta/3600)%24;
	delta -= hours*3600;
	// calculate (and subtract) whole minutes
	var minutes = Math.floor(delta/60)%60;
	delta -= minutes*60;
	// what's left is seconds
	var seconds = Math.floor(delta%60);  // in theory the modulus is not required
	return(days+":"+hours+":"+minutes+":"+seconds);
}

function refreshTimes(){
	let now = Date.now();$(".timerField").each(function(index){
		$(this).find("b").html(getTimeDiff(now,$(this).find(".maxtime").html()));
		if($(this).find("b").html()=="0:0:0:0"){$(this).find("b").css("color","green")}
		else{$(this).find("b").css("color","black")}
	});
}

$(document).ready(function(){
	console.log("READY");
  $("#addTimerForm").submit(function(e){
	e.preventDefault();
	let delim = ($('#timeDelimiter option').filter(':selected').val()*$('#fnumber').val()*1000);
	let maxt = (delim+Date.now());
	$("#content").append('<div class="timerField">'+$("#fname").val()+'<b>'+getTimeDiff(Date.now(),maxt)+'</b><i class="invis timeDelim" style="display:none;">'+delim+'</i>     <i class="invis maxtime" style="display:none;">'+maxt+'</i><button class="resetButton">Reset</button><button class="deleteButton">X</button></div>');
	$("#content .timerField:last-child .deleteButton").click(function(){$(this).parent().remove();});
	$("#content .timerField:last-child .resetButton").click(function(){$(this).parent().find(".maxtime").html(Date.now()+parseInt($(this).parent().find(".timeDelim").html()));refreshTimes();});
  });
  window.setInterval(refreshTimes, 1000);
});

